#!/bin/bash
set -e

#echo "Preparing for APP_DEVELOPMENT: ${APP_DEVELOPMENT}?"
## Automatic deploy for a development - not depending on the FLASK_ENV
#if [[ "$APP_DEVELOPMENT" = 'yes' ]]; then
##    apt-get update && apt-get install -y netcat
#    until nc -z -v -w30 $APP_DB_HOSTNAME 3306
#    do
#      echo "Waiting for database connection..."
#      # wait for 5 seconds before check again
#      sleep 5
#    done
#    sleep 5
#    echo "Waiting for potential database initialization..."
#    gosu pzmau flask deploy
#fi

echo "Preparing for ENV ${FLASK_ENV}"
# Allow DEV
if [[ "$FLASK_ENV" = 'development' ]]; then
    /home/api/venv/bin/python -m pip install -r requirements/development.txt
fi
# Allow TEST
if [[ "$FLASK_ENV" = 'test' ]]; then
    /home/api/venv/bin/python -m pip install -r requirements/development.txt
fi

# Allow the user to run arbitrarily commands like bash
exec "$@"